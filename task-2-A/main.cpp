#include <iostream>
#include <vector>

using std::size_t;

typedef std::vector<size_t> Size_t_vector;

const int ALPAHABET_SIZE = 26;

class SuffixArrayBuilder {
public:
    explicit SuffixArrayBuilder(const std::string& str) : extended_str_(str + '`') {}

    Size_t_vector  buildSuffixArray();
private:
    // рассматриваем новую строку s1 = s + '`', где '`' - символ меньший любого символа строки s
    const std::string extended_str_;
    Size_t_vector shifts_;
    Size_t_vector equivalence_class_;

    void countSortBySingleSymbol(const size_t prefix_length, size_t& amount_of_equivalence_classes);
    void countSortUsingPrevious(const size_t sorting_segment_length, size_t& amount_of_equivalence_classes);
    void sort();
};


Size_t_vector SuffixArrayBuilder::buildSuffixArray() {
    // все суффиксы содержатся в циклических сдвигах extended_str_ до символа '`', отсортируем циклические сдвиги
    // shifts[i] = номер циклического сдвига (назад)
    Size_t_vector shifts(extended_str_.length());
    for (size_t index = 0; index < shifts.size(); ++index) {
        shifts[index] = index;
    }
    // на Kом шаге сортировки equivalence_class[index] = класс эквивалентности относительно равенства префикса сдвига shift[index] от 0 до sorting_segment_length - 1
    // shift[index1][0:sorting_segment_length] > shift[index2][0:sorting_segment_length] => equivalence_class[index1] > equivalence_class[index2] после сортировки по префиксам длины sorting_segment_length
    Size_t_vector equivalence_class(extended_str_.length(), 0);
    sort();
    return shifts;
}

void SuffixArrayBuilder::sort(){
    size_t amount_of_equivalence_classes = 1;
    countSortBySingleSymbol(0, amount_of_equivalence_classes);

    size_t prefix_length = 0;
    size_t sorting_segment_length = 1; // длина подстроки, по которой сортируем
    while (prefix_length + sorting_segment_length != extended_str_.length()) {
        sorting_segment_length *= 2;
        if (prefix_length + sorting_segment_length > extended_str_.length()) {
            prefix_length += sorting_segment_length / 2;
            sorting_segment_length = 1;
            countSortBySingleSymbol(prefix_length, amount_of_equivalence_classes);
            continue;
        }
        countSortUsingPrevious(sorting_segment_length, amount_of_equivalence_classes);
    }
}

void SuffixArrayBuilder::countSortBySingleSymbol(const size_t prefix_length, size_t& amount_of_equivalence_classes) {
    // quantity_shift - в сортировке подсчетом сначала для каждого 'символа' считаем количество вхождений в вектор,
    // потом, проходя по quantity_shift от меньшего символа к большему, вычисляем место вставки отрезка одинаковых символов
    std::vector<Size_t_vector> quantity_shift_per_class(amount_of_equivalence_classes);

    for (auto&& quantity : quantity_shift_per_class) {
        quantity.assign(ALPAHABET_SIZE + 1, 0);
    }

    // подсчет количества вхождений
    for (auto shift : shifts_) {
        ++quantity_shift_per_class[ equivalence_class_[shift] ][ extended_str_[(prefix_length + shift) % extended_str_.length()] - '`' ];
    }

    // вычисление места вставки отрезка одинаковых символов
    size_t shift_in_sorted = 0;
    for (auto&& quantity_shift : quantity_shift_per_class){
        for (size_t in_class_index = 0; in_class_index < quantity_shift.size(); ++in_class_index) {
            size_t tmp = shift_in_sorted;
            shift_in_sorted += quantity_shift[in_class_index];
            quantity_shift[in_class_index] = tmp;
        }
    }

    std::vector<size_t> old_shifts = shifts_;
    for (size_t index = 0; index < shifts_.size(); ++index) {
        size_t old_shift = old_shifts[index];
        auto&& eq_class_shift = quantity_shift_per_class[ equivalence_class_[old_shift] ];
        // вставка сдвига на нужное место в соответствии с вычисленными местами вставок,
        // смещение места вставки для следующего сдвига из этого же класса эквивалентности
        shifts_[eq_class_shift[ extended_str_[(prefix_length + old_shift) % extended_str_.length()] - '`' ]++] = old_shift;
    }

    size_t equivalence_class_num = 0;
    equivalence_class_[shifts_[0]] = equivalence_class_num;
    for (size_t index = 1; index < shifts_.size(); ++index) {
        if (extended_str_[shifts_[index - 1]] != extended_str_[shifts_[index]]) {
            ++equivalence_class_num;
        }
        equivalence_class_[shifts_[index]] = equivalence_class_num;
    }
    amount_of_equivalence_classes = equivalence_class_num + 1;
}


// prefix_length - длина префикса, по которому циклические сдвиги уже отсортированы
void SuffixArrayBuilder::countSortUsingPrevious(const size_t sorting_segment_length, size_t& amount_of_equivalence_classes) {
    std::vector<Size_t_vector> quantity_shift_per_class(amount_of_equivalence_classes);
    for (auto&& quantity : quantity_shift_per_class) {
        quantity.assign(amount_of_equivalence_classes, 0);
    }

    for (size_t shift : shifts_) {
        ++quantity_shift_per_class[ equivalence_class_[shift] ][ equivalence_class_[(shift + sorting_segment_length / 2) % extended_str_.length()] ];
    }

    size_t shift_in_sorted = 0;
    for (size_t index = 0; index < quantity_shift_per_class.size(); ++index) {
        Size_t_vector & quantity_shift = quantity_shift_per_class[index];
        for (size_t in_class_index = 0; in_class_index < quantity_shift.size(); ++in_class_index) {
            size_t tmp = shift_in_sorted;
            shift_in_sorted += quantity_shift[in_class_index];
            quantity_shift[in_class_index] = tmp;
        }
    }

    std::vector<size_t> old_shifts = shifts_;
    for (size_t index = 0; index < shifts_.size(); ++index) {
        size_t old_shift = old_shifts[index];
        auto&& eq_class_shift = quantity_shift_per_class[ equivalence_class_[old_shift] ];
        // вставка сдвига на нужное место в соответствии с вычисленными местами вставок,
        // смещение места вставки для следующего сдвига из этого же класса эквивалентности
        shifts_[eq_class_shift[ equivalence_class_[(old_shift + sorting_segment_length / 2) % extended_str_.length()] ]++] = old_shift;
    }

    Size_t_vector old_equivalence_class = equivalence_class_;
    size_t equivalence_class_num = 0;
    equivalence_class_[shifts_[0]] = equivalence_class_num;
    for (size_t index = 1; index < shifts_.size(); ++index) {
        size_t old_eq_class_index = (shifts_[index - 1] + sorting_segment_length / 2) % extended_str_.length();
        size_t next_old_eq_class_index = (shifts_[index] + sorting_segment_length / 2) % extended_str_.length();
        // увеличение номера класса эквивалентности в случае, если очередной сдвиг не лежит с предыдущим в одном классе
        if (old_equivalence_class[shifts_[index - 1]] != old_equivalence_class[shifts_[index]]
            || old_equivalence_class[old_eq_class_index] != old_equivalence_class[next_old_eq_class_index]) {
            ++equivalence_class_num;
        }
        equivalence_class_[shifts_[index]] = equivalence_class_num;
    }
    amount_of_equivalence_classes = equivalence_class_num + 1;
}

// считает наибольший общий префикс соседних суффиксов алгоритмом Касаи
std::vector<int> countNearLcp(const Size_t_vector  &suffix_array, const std::string &str) {
    Size_t_vector  ranks(suffix_array.size()); // бывший inverse_suffix_array
    std::vector<int> lcp(suffix_array.size());

    // инициализируем массив рангов
    for (size_t index = 0; index < suffix_array.size(); ++index) {
        ranks[suffix_array[index]] = index;
    }

    size_t current_lcp = 0;
    for (size_t suffix_index = 0; suffix_index < str.length() + 1; ++suffix_index) {
        // массив рангов на 1 больше суффиксного массива. алгоритм Касаи
        // перебирает строки по порядку, но "дырку" в суффиксном массиве обрабатываем вручную.
        if (ranks[suffix_index] == str.length()) {
            lcp[str.length()] = -1;
            current_lcp = 0;
            continue;
        }

        // сделующий по рангу суффикс
        auto &&next_suffix_index = suffix_array[ranks[suffix_index] + 1];

        while (
                std::max(suffix_index, next_suffix_index) + current_lcp < str.length() + 1 &&
                str[suffix_index + current_lcp] == str[next_suffix_index + current_lcp]
                ) {
            ++current_lcp;
        }
        lcp[ranks[suffix_index]] = current_lcp;
        if (current_lcp > 0) {
            --current_lcp;
        }
    }
    return lcp;
}

size_t getSubstringsAmount(const std::string& str) {
    size_t substringsAmount = 0;
    SuffixArrayBuilder builder(str);
    Size_t_vector suffixArray = builder.buildSuffixArray();
    std::vector<int> lcp = countNearLcp(suffixArray, str);
    for (size_t index = 1; index < suffixArray.size(); ++index) {
        substringsAmount += str.length() - suffixArray[index] - lcp[index - 1];
    }
    return substringsAmount;
}

int main() {
    std::string str;
    std::cin >> str;
    std::cout << getSubstringsAmount(str) << '\n';
}
