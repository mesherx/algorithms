#include <iostream>
#include <string>
#include <vector>


//Cчитает z функцию для строки pattern
std::vector<std::size_t> calculateZfunctionForPattern(const std::string& pattern) {
    std::vector<std::size_t> z_function(pattern.size());
    z_function[0] = 0;
    std::size_t left = 0, right = 0;
    for (std::size_t index = 1; index < pattern.size(); ++index) {
        std::size_t matched_length;
        if (index > right) {
            matched_length = 0;
        } else {
            matched_length = std::min(z_function[index - left], right - index + 1);
        }
        while (index + matched_length < pattern.size() && pattern[matched_length] == pattern[index + matched_length]) {
            ++matched_length;
        }
        z_function[index] = matched_length;
        if (matched_length > 0 && index + matched_length - 1 > right) {
            right = index + matched_length - 1;
            left = index;
        }
    }
    return z_function;
}


/*
Считает z функцию для строки pattern методом calculateZfunctionForPattern,
используя её считает псевдо z функцию для строки pattern+text
(совпадает с z функцией для строки pattern+$+text).
Z функция == pattern.size() (подстрока в text совпала с pattern) =>
=> добавляем начало подстроки в ответ, прекращаем подсчет z функции для текущего index
(дальнейшее совпадение не интересно).
*/
std::vector<std::size_t> findAllOccurences(const std::string& pattern, const std::string& text) {
    std::vector<std::size_t> z_function = calculateZfunctionForPattern(pattern);
    std::vector<std::size_t> positions;
    std::size_t right = 0, left = 0;
    for (std::size_t index = 0; index < text.size(); ++index) {
        std::size_t matched_length; 
        if (index > right) {
            matched_length = 0;
        } else {
            matched_length = std::min(z_function[index - left], right - index + 1);
        }
        while (matched_length < pattern.size() && index + matched_length < text.size() && pattern[matched_length] == text[index + matched_length]) {
            ++matched_length;
        }
        if (matched_length == pattern.size()) {
            positions.push_back(index);
        }
        if (matched_length > 0 && index + matched_length - 1 > right) {
            right = index + matched_length - 1;
            left = index;
        }
    }
    return positions;
 }


int main() {
    std::string text, pattern;
    std::cin >> pattern >> text;
    std::vector<std::size_t> positions = findAllOccurences(pattern, text);
    for (auto pos : positions) {
        std::cout << pos << ' ';
    }
    std::cout << std::endl;
    return 0;
}
