#include <iostream>
#include <vector>
#include <set>
#include <sstream>


std::vector<std::size_t> calculateZFunction(const std::string& str) {
    std::vector<std::size_t> z_function(str.size());
    z_function[0] = 0;
    std::size_t left = 0, right = 0;
    for (std::size_t index = 1; index < str.size(); ++index) {
        std::size_t j;
        if (index > right) {
            j = 0;
        } else {
            j = std::min(z_function[index - left], right - index + 1);
        }
        while (index + j < str.size() && str[j] == str[index + j]) {
            ++j;
        }
        z_function[index] = j;
        if (j > 0 && index + j - 1 > right) {
            right = index + j - 1;
            left = index;
        }
    }
    return z_function;
}


std::vector<std::size_t> calculatePrefixFunction(const std::string& str) {
    std::vector<std::size_t> prefix_function(str.size());
    prefix_function[0] = 0;
    for (std::size_t index = 1; index < str.size(); ++index) {
        std::size_t index_to_match = prefix_function[index - 1];
        while (index_to_match != 0 && str[index_to_match] != str[index]) {
            index_to_match = prefix_function[index_to_match - 1];
        }
        if (index_to_match != 0) {
            prefix_function[index] = index_to_match + 1;
        } else {
            prefix_function[index] = (str[0] == str[index]) ? 1 : 0;
        }
    }
    return prefix_function;
}


std::string generateStringFromPrefixFunction(const std::vector<std::size_t>& prefix_function) {
    std::string str;
    str.resize(prefix_function.size());
    if (prefix_function.size() == 0) {
        return str;
    }
    str[0] = 'a';
    /*
    Префикс функция не равна нулю - копируем соответсвующее значение.
    Префикс функция равна нулю - запоминаем символы, идущие за префиксами,
    заканчивающимися перед незаданным пока символом. Выбираем минимальный
    из допустимых символ.
    */
    for (std::size_t index = 1; index < prefix_function.size(); ++index) {
        if (prefix_function[index] > 0) {
            str[index] = str[prefix_function[index] - 1];
        } else {
            std::set<char> forbidden;
            std::size_t forbidden_index = index;
            while (forbidden_index > 0) {
                forbidden_index = prefix_function[forbidden_index - 1];
                forbidden.insert(str[forbidden_index]);
            }
            char symbol = 'b';
            while (forbidden.count(symbol) != 0) {
                ++symbol;
            }
            str[index] = symbol;
        }
    }
    return str;
}


std::string generateStringFromZFunction(const std::vector<std::size_t>& z_function) {
    std::string str;
    str.resize(z_function.size());
    if (z_function.size() == 0) {
        return str;
    }
    str[0] = 'a';
    /*
    значение z_function[index] = length больше нуля - поэлементно копируем
    префикс длины length в соответствующие позиции index + j, если
    1) z_function[index + j] == length - j, т.е. символ y за рассматриваемым
    отрезком длины length не совпадает с символом q = str[z_function[index + j]],
    добавляем его как запрещенный для будущего определения символа y.
    2) z_function[index + j] > length - j => переопределяем копируемый префикс.

    значение z_function[index] = length = 0 - выбираем минимальный символ из допустимых.
    */
    std::set<char> forbidden;
    std::size_t index = 1;
    while (index < z_function.size()) {
        if (z_function[index] > 0) {
            std::size_t length = z_function[index];
            std::size_t j = 0;
            while (j < length) {
                if (z_function[index + j] > length - j) {
                    index = index + j;
                    length = z_function[index];
                    j = 0;
                    forbidden.clear();
                } else {
                    if (z_function[index + j] == length - j) {
                        forbidden.insert(str[z_function[index + j]]);
                    }
                    str[index + j] = str[j];
                    ++j;
                }
            }
            index = index + j;
        } else {
            char symbol = 'b';
            while (forbidden.count(symbol) != 0) {
                ++symbol;
            }
            str[index] = symbol;
            forbidden.clear();
            ++index;
        }
    }
    return str;
}


std::vector<std::size_t> getZFunctionFromPrefixFunction(const std::vector<std::size_t>& prefix_function) {
    std::vector<std::size_t> z_function(prefix_function.size(), 0);
    z_function[0] = 0;
    /*
    Задаем минимальное значение z функции в некоторых точках.
    В z_function[1] получим корректное значение(будет рассмотрено соответствующее
    значение префикс функции)
    */
    for (std::size_t index = 1; index < prefix_function.size(); ++index) {
        if (prefix_function[index] != 0) {
            z_function[index - prefix_function[index] + 1] = prefix_function[index];
        }
    }
    /*
    увеличиваем z функцию если можно
    */
    std::size_t index = 1;
    while (index < z_function.size()) {
        std::size_t substring_length = z_function[index];
        std::size_t new_index = index;
        if (substring_length > 0) {
            for (std::size_t inner_index = 1; inner_index < substring_length; ++inner_index) {
                if (z_function[index + inner_index] > z_function[inner_index]) {
                    break;
                }
                if (substring_length <= inner_index + z_function[inner_index]) {
                    z_function[index + inner_index] = substring_length - inner_index;
                } else if (z_function[index + inner_index] < z_function[inner_index]) {
                    z_function[index + inner_index] = z_function[inner_index];
                }
                new_index = index + inner_index;
            }
        }
        index = new_index + 1;
    }
    return z_function;
}


std::vector<std::size_t> getPrefixFunctionFromZFunction(const std::vector<std::size_t>& z_function) {
    std::vector<std::size_t> prefix_function(z_function.size());
    if (z_function.size() == 0) {
        return prefix_function;
    }
    prefix_function[0] = 0;
    /*
    Найдя непустой префикс суффикса, заполняем соответствующие значения
    префикс функции, если они еще не заданы. Так как идём по z функции
    слева направо, то верное значение префикс функции задается первым присваиванием
    (следующие присваивания соответсвуют элементу ZFunction с большим index,
     а значит значение префикс функции может лишь уменьшиться)
     */
    for (std::size_t index = 1; index < z_function.size(); ++index) {
        if (z_function[index] != 0) {
            std::size_t inner_index = z_function[index] - 1;
            while (prefix_function[index + inner_index] == 0 && inner_index != 0) {
                prefix_function[index + inner_index] = inner_index + 1;
                --inner_index;
            }
        }
    }
    return prefix_function;
}

int main() {
    return 0;
}