#include <iostream>
#include <string>
#include <vector>

class BigInteger {
private:
    std::vector<int> digits_;
    bool sign_;
    static const int BASE_ = 1e4;

    bool equalsZero() const {
        return !*this;
    }

public:
    BigInteger() : sign_(true) {}

    BigInteger(int x) {
        sign_ = (x >= 0);
        if (!sign_) {
            x *= -1;
        }
        if (x == 0) {
            digits_.push_back(0);
        }
        while (x != 0) {
            digits_.push_back(x % BASE_);
            x /= BASE_;
        }
    }

    BigInteger& operator+=(const BigInteger& x) {
        if (sign_ == x.sign_) {
            int overflow = 0;

            for (size_t index = 0; index < std::min(digits_.size(), x.digits_.size()); ++index) {
                int old_digits_index_value = digits_[index];
                digits_[index] = (x.digits_[index] + digits_[index] + overflow) % BASE_;
                overflow = (x.digits_[index] + old_digits_index_value + overflow) / BASE_;
            }

            for (size_t index = std::min(digits_.size(), x.digits_.size()); index < digits_.size(); ++index) {
                int old_digits_index_value = digits_[index];
                digits_[index] = (digits_[index] + overflow) % BASE_;
                overflow = (old_digits_index_value + overflow) / BASE_;
            }

            for (size_t index = std::min(digits_.size(), x.digits_.size()); index < x.digits_.size(); ++index) {
                digits_.push_back((x.digits_[index] + overflow) % BASE_);
                overflow = (x.digits_[index] + overflow) / BASE_;
            }

            if (overflow != 0) {
                digits_.push_back(overflow);
            }
        } else {
            BigInteger result;

            if ((!sign_ && *this > -x) || (sign_ && *this < -x)) {
                result = x;
                sign_ = !sign_;

                for (size_t index = 0; index < digits_.size(); ++index) {
                    if (digits_[index] > result.digits_[index]) {
                        size_t borrow_index;
                        for (borrow_index = index + 1; borrow_index < result.digits_.size() && result.digits_[borrow_index] == 0; ++borrow_index);
                        --result.digits_[borrow_index];
                        for (size_t _index = borrow_index - 1; _index > index; --_index) {
                            result.digits_[_index] = BASE_ - 1;
                        }
                        result.digits_[index] += BASE_ - digits_[index];
                    } else {
                        result.digits_[index] -= digits_[index];
                    }
                }
            } else {
                result = *this;

                for (size_t index = 0; index < x.digits_.size(); ++index) {
                    if (result.digits_[index] < x.digits_[index]) {
                        size_t borrow_index;
                        for (borrow_index = index + 1; borrow_index < result.digits_.size() && result.digits_[borrow_index] == 0; ++borrow_index);
                        --result.digits_[borrow_index];
                        for (size_t _index = borrow_index - 1; _index > index; --_index) {
                            result.digits_[_index] = BASE_ - 1;
                        }
                        result.digits_[index] = BASE_ - x.digits_[index] + result.digits_[index];
                    } else {
                        result.digits_[index] -= x.digits_[index];
                    }
                }
            }

            digits_.clear();
            size_t index;
            for (index = result.digits_.size() - 1; index != 0 && result.digits_[index] == 0; --index);
            for (size_t _index = 0; _index <= index; ++_index) {
                digits_.push_back(result.digits_[_index]);
            }
        }

        if (equalsZero()) {
            sign_ = true;
        }

        return *this;
    }

    BigInteger& operator-=(const BigInteger& x) {
        *this += -x;
        return *this;
    }

    BigInteger& operator*=(const BigInteger& x) {
        if (equalsZero() || x.equalsZero()) {
            *this = 0;
            return *this;
        }

        BigInteger answer = 0;

        for (size_t index = 0; index < x.digits_.size(); ++index) {
            if (x.digits_[index] != 0) {
                BigInteger result;
                result.digits_.clear();
                for (size_t _index = 0; _index < index; ++_index) {
                    result.digits_.push_back(0);
                }

                for (size_t _index = index; _index < index + digits_.size(); ++_index) {
                    result.digits_.push_back(digits_[_index - index]);
                }

                int overflow = 0;
                for (size_t _index = 0; _index < digits_.size(); ++_index) {
                    int old_result_digits_value = result.digits_[_index + index];
                    result.digits_[_index + index] = (result.digits_[_index + index] * x.digits_[index] + overflow) % BASE_;
                    overflow = (old_result_digits_value * x.digits_[index] + overflow) / BASE_;
                }

                if (overflow != 0) {
                    result.digits_.push_back(overflow);
                }

                answer += result;
            }
        }

        bool sign = (sign_ == x.sign_);
        *this = answer;
        sign_ = sign;
        return *this;
    }

    BigInteger& operator/=(const BigInteger& x) {
        if (equalsZero()) {
            return *this;
        }

        BigInteger result = 0, divider = x;
        std::vector<int> answer;
        divider.sign_ = true;

        size_t pos = digits_.size();
        while (true) {
            --pos;

            if (not result.equalsZero()) {
                result.digits_.push_back(0);
                for (size_t index = result.digits_.size() - 1; index >= 1; --index) {
                    result.digits_[index] = result.digits_[index - 1];
                }
            }
            result.digits_[0] = digits_[pos];

            if (answer.size() != 1 || answer[0] != 0) {
                answer.push_back(0);
            }

            while (result >= divider) {
                result -= divider;
                ++answer[answer.size() - 1];
            }

            if (pos == 0) {
                break;
            }
        }

        digits_.clear();
        for (size_t index = 0; index < answer.size(); ++index) {
            digits_.push_back(answer[answer.size() - 1 - index]);
        }

        if (equalsZero()) {
            sign_ = true;
        } else {
            sign_ = (sign_ == x.sign_);
        }

        return *this;
    }

    BigInteger& operator%=(const BigInteger& x) {
        if (equalsZero()) {
            return *this;
        }

        BigInteger result = 0, divider = x;

        divider.sign_ = true;
        size_t pos = digits_.size();
        while(true) {
            --pos;

            if (not result.equalsZero()) {
                result.digits_.push_back(0);
                for (size_t index = result.digits_.size() - 1; index >= 1; --index) {
                    result.digits_[index] = result.digits_[index - 1];
                }
            }
            result.digits_[0] = digits_[pos];

            while (result >= divider) {
                result -= divider;
            }

            if (pos == 0) {
                break;
            }
        }

        bool sign = sign_;
        *this = result;
        sign_ = sign;
        if (equalsZero()) {
            sign_ = true;
        }

        return *this;
    }

    const BigInteger operator-() const {
        if (equalsZero()) {
            return *this;
        } else {
            BigInteger result = *this;
            result.sign_ = !result.sign_;
            return result;
        }
    }

    BigInteger& operator--() {
        *this -= 1;
        return *this;
    }

    const BigInteger operator--(int) {
        BigInteger old_value = *this;
        *this -= 1;
        return old_value;
    }

    BigInteger& operator++() {
        *this += 1;
        return *this;
    }

    const BigInteger operator++(int) {
        BigInteger old_value = *this;
        *this += 1;
        return old_value;
    }

    bool operator==(const BigInteger& x) const {
        if (sign_ != x.sign_ || digits_.size() != x.digits_.size()) {
            return false;
        }

        for (size_t index = 0; index < digits_.size(); ++index) {
            if (digits_[index] != x.digits_[index]) {
                return false;
            }
        }
        return true;
    }

    bool operator!=(const BigInteger& x) const {
        return !(*this == x);
    }

    bool operator<(const BigInteger& x) const {
        if (sign_ != x.sign_) {
            return sign_ < x.sign_;
        }
        if (digits_.size() < x.digits_.size()) {
            return sign_;
        }
        if (digits_.size() > x.digits_.size()) {
            return !sign_;
        }

        for (size_t index = digits_.size() - 1; index != 0; --index) {
            if (digits_[index] < x.digits_[index]) {
                return sign_;
            }
            if (digits_[index] > x.digits_[index]) {
                return !sign_;
            }
        }
        if (digits_[0] < x.digits_[0]) {
            return sign_;
        }
        if (digits_[0] > x.digits_[0]) {
            return !sign_;
        }
        return false;
    }

    bool operator>(const BigInteger& x) const {
        return x < *this;
    }

    bool operator<=(const BigInteger& x) const {
        return !(*this > x);
    }

    bool operator>=(const BigInteger& x) const {
        return !(*this < x);
    }

    friend std::istream& operator>>(std::istream& input_stream, BigInteger& x);

    friend std::ostream& operator<<(std::ostream& out_stream, const BigInteger& x);

    std::string toString() const {
        std::string str;

        if (!sign_) {
            str.push_back('-');
        }

        if (equalsZero()) {
            str.push_back('0');
            return str;
        }
        bool isNumberGreaterZero = false;
        size_t pos = digits_.size();
        while(true) {
            --pos;
            int digit = digits_[pos];
            int divider = BASE_ / 10;
            while (divider) {
                if (pos != digits_.size() - 1 || digit / divider || isNumberGreaterZero) {
                    str.push_back(digit / divider + '0');
                    isNumberGreaterZero = true;
                }
                digit %= divider;
                divider /= 10;
            }
            if (pos == 0) {
                break;
            }
        }
        return str;
    }

    explicit operator bool() const {
        return !(digits_.size() == 1 && digits_[0] == 0);
    }

    BigInteger& operator=(const BigInteger& x) {
        if (this != &x) {
            digits_.clear();
            sign_ = x.sign_;
            for (size_t index = 0; index < x.digits_.size(); ++index) {
                digits_.push_back(x.digits_[index]);
            }
        }
        return *this;
    }
};


const BigInteger operator+(const BigInteger& one, const BigInteger& another) {
    BigInteger result = one;
    result += another;
    return result;
}

const BigInteger operator-(const BigInteger& minuend, const BigInteger& subtrahend) {
    BigInteger result = minuend;
    result -= subtrahend;
    return result;
}

const BigInteger operator*(const BigInteger& one, const BigInteger& another) {
    BigInteger result = one;
    result *= another;
    return result;
}

const BigInteger operator/(const BigInteger& dividend, const BigInteger& divider) {
    BigInteger result = dividend;
    result /= divider;
    return result;
}

const BigInteger operator%(const BigInteger& dividend, const BigInteger& divider) {
    BigInteger result = dividend;
    result %= divider;
    return result;
}

std::ostream& operator<<(std::ostream& out_stream, const BigInteger& x) {
    out_stream << x.toString();
    return out_stream;
}

std::istream& operator>>(std::istream& input_stream, BigInteger& x) {
    x.digits_.clear();

    std::string input;
    input_stream >> input;

    x.sign_ = input[0] != '-';

    size_t pos = input.size() + 3;

    do {
        pos -= 4;
        int digit = 0;
        if (x.sign_ || pos > 0) {
            digit += input[pos] - '0';
        }
        if (pos >= 1 && (x.sign_ || pos > 1)) {
            digit += (input[pos - 1] - '0') * 10;
        }
        if (pos >= 2 && (x.sign_ || pos > 2)) {
            digit += (input[pos - 2] - '0') * 100;
        }
        if (pos >= 3 && (x.sign_ || pos > 3)) {
            digit += (input[pos - 3] - '0') * 1000;
        }
        if ((pos >= 4 && (x.sign_ || pos > 4)) || digit) {
            x.digits_.push_back(digit);
        }

    } while (pos > 3);

    if (x.digits_.size() == 0) {
        x.digits_.push_back(0);
    }

    return input_stream;
}
